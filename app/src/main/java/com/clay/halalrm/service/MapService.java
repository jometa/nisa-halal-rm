package com.clay.halalrm.service;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;

import com.clay.halalrm.tools.requestHandler;
import com.clay.informhalal.geoCode;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapService {
    public final Context context;
    public final String key;

    public MapService(Context context, String key) {
        this.context = context;
        this.key = key;
    }

    public String getFormattedAdress(double latitude, double longitude) {
        final Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("maps.googleapis.com")
                .appendPath("maps")
                .appendPath("api")
                .appendPath("geocode")
                .appendPath("json")
                .appendQueryParameter("latlng", latitude+","+longitude)
                .appendQueryParameter("key", key);
        final String string = builder.build().toString();
//        System.out.println("string = " + string);
        final String rest = requestHandler.INSTANCE.readingRest(context, string);
//        System.out.println("rest = " + rest);

        final Gson gson = new Gson();
        final geoCode formatted_address = gson.fromJson(rest, geoCode.class);
        Log.d("data.status", formatted_address.getStatus());

        if (formatted_address.getStatus().equals("OK")) {
            final String address = formatted_address.getResults().get(0).getFormatted_address();
            System.out.println("address = " + address);
            return address;
        }

        return "";
    }

    public String getAddress(Context context, double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
            for (Address ad1 : addresses) {
                System.out.println(ad1);
            }
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getAddressLine(0));
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }
}
