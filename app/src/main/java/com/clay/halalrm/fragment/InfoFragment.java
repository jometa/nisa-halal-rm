package com.clay.halalrm.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clay.halalrm.MyUtil;
import com.clay.halalrm.R;
import com.clay.halalrm.fragment.dummy.ModelObject;
import com.clay.halalrm.model.RumahMakan;
import com.clay.halalrm.service.MapService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Boolean  mAdmin;
    private long     mKey;
    private RumahMakan rumahMakan;
    private String formattedAddress;
    private double myLat,myLng;
    List<Bitmap> imageURL = new LinkedList<>();
    ViewPager viewPager = null;
    private OnFragmentInteractionListener mListener;
    public MapService mapService;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(
            boolean param1,
            long param2,
            double lat,
            double lng
    ) {

        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, param1);
        args.putLong(ARG_PARAM2, param2);
        args.putDouble("lat",lat);
        args.putDouble("lng",lng);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("getArguments() = " + getArguments());
        if (getArguments() != null) {
            mAdmin = getArguments().getBoolean(ARG_PARAM1,false);
            mKey = getArguments().getLong(ARG_PARAM2,0l);
            myLat = getArguments().getDouble("lat");
            myLng = getArguments().getDouble("lng");
            rumahMakan = RumahMakan.findById(RumahMakan.class,mKey);
        }
        System.out.println("InfoFragment.rumahMakan = " + rumahMakan);

        String key = getResources().getString(R.string.google_maps_key);
        Context context = getContext();
        this.mapService = new MapService(context, key);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        TextView textAlamat= (TextView) view.findViewById(R.id.textAlamat);
        TextView textRating= (TextView) view.findViewById(R.id.textRating);
        TextView textKode= (TextView) view.findViewById(R.id.textKode);
        TextView textNamaRM= (TextView) view.findViewById(R.id.textNamaRM);
        TextView textJenis= (TextView) view.findViewById(R.id.textJenis);

        final Button buttonJalur = (Button) view.findViewById(R.id.buttonJalur);
        final Button buttonOpenMap = (Button) view.findViewById(R.id.buttonOpenMap);

        buttonOpenMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenMap();
            }
        });
        buttonJalur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenJalur();
            }
        });

        if (rumahMakan.getFormatted_address() == null || rumahMakan.getFormatted_address().isEmpty()) {
            formattedAddress = this.mapService.getFormattedAdress(rumahMakan.getLat(), rumahMakan.getLng());
            textAlamat.setText(formattedAddress);
        } else {
            formattedAddress = rumahMakan.getFormatted_address();
            textAlamat.setText(rumahMakan.getFormatted_address());
        }

        textRating.setText(rumahMakan.getRating().toString());
        textKode.setText(rumahMakan.getCompound_code());
        textNamaRM.setText(rumahMakan.getName());

        String global_code = rumahMakan.getGlobal_code();
        switch (global_code) {
            case "RMjawa.json":
                textJenis.setText("Jawa");
                break;
            case "RMmadura.json":
                textJenis.setText("Madura");
                break;
            case "RMpadang.json":
                textJenis.setText("Padang");
                break;
        }


        List<Bitmap> imageURL = new LinkedList<>();
        System.out.println("Before load image");

        AsyncImageLoad asyncImageLoad = new AsyncImageLoad();
        ArrayList<String> imageList = new ArrayList<>();
        if (rumahMakan.getFoto1() != null) {
            imageList.add(rumahMakan.getFoto1());
        }
        if (rumahMakan.getFoto2() != null) {
            imageList.add(rumahMakan.getFoto2());
        }
        if (rumahMakan.getFoto3() != null) {
            imageList.add(rumahMakan.getFoto3());
        }

        asyncImageLoad.execute(imageList.toArray(new String[imageList.size()]));


        this.viewPager = (ViewPager) view.findViewById(R.id.vpRumahMakan);
        this.viewPager.setAdapter(new CustomPagerAdapter(getContext(), imageURL));

        return view;
    }

    private void OpenJalur() {

        final String format = String.format(
                Locale.ENGLISH,
                "http://maps.google.com/maps?saddr=%f,%f&daddr=%f,%f",
                myLat,
                myLng,
                rumahMakan.getLat(),
                rumahMakan.getLng()
        );
        System.out.println("format = " + format);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(format));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

    private void OpenMap() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("search")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("query", rumahMakan.getName());
//                .appendQueryParameter("query_place_id", intent.getStringExtra("id"));

        final String uriString = String.format(
            Locale.ENGLISH,
            "http://maps.google.com/maps?q=%f,%f(%s)&z=15",
                rumahMakan.getLat(),
                rumahMakan.getLng(),
                rumahMakan.getName()
        );
//        String uriFormat = String.format("geo:%f,%f", rumahMakan.getLat(), rumahMakan.getLng());
        Uri locationUri = Uri.parse(uriString);

        Uri mainUri = rumahMakan.getInGoogle() ? Uri.parse(builder.build().toString()) : locationUri;

//        String key = builder.build().toString();
        Intent intent = new Intent(Intent.ACTION_VIEW, mainUri);
        startActivity(intent);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;
        private List<Bitmap> mImageURL;

        public CustomPagerAdapter(Context context, List<Bitmap> imageURL) {
            mContext = context;
            mImageURL = imageURL;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            final ModelObject modelObject = ModelObject.values()[position];
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);

            final ImageView imageView = layout.findViewById(modelObject.getImageViewId());
            collection.addView(layout);


            try {
                Bitmap bitmap = mImageURL.get(position);
                if (bitmap != null)
                {
                    imageView.setImageBitmap(mImageURL.get(position));
                }
            }
            catch (Exception E) {}

            layout.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    //this will log the page number that was click
                    Log.i("TAG", "This page was clicked: " + position);

                    if (mAdmin)
                        SimpanGambar(position);

                }
            });
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return ModelObject.values().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mContext.getString(R.string.app_name);
        }
    }

    private void SimpanGambar(int position) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra("position", position);
        intent.setAction(Intent.ACTION_GET_CONTENT);

        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), position);
    }


    public class AsyncImageLoad extends AsyncTask<String, Void, List<Bitmap>> {

        @Override
        protected List<Bitmap> doInBackground(String... strings) {
            List<Bitmap> bitmaps = new ArrayList<>();
            for (String url: strings) {
                Bitmap bitmapFromURL = MyUtil.getBitmapFromURL(url);
                bitmaps.add(bitmapFromURL);
            }
            return bitmaps;
        }

        @Override
        protected void onPostExecute(List<Bitmap> bitmaps) {
            if (bitmaps.size() == 0) {
                return;
            }

            System.out.println("bitmaps.size = " + bitmaps.size());

            imageURL.addAll(bitmaps);
            viewPager.setAdapter(new CustomPagerAdapter(getContext(), imageURL));
        }
    }


}
